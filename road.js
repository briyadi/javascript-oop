let sedan = new Car('Honda Jazz', 150)
let becak = new Bicycle('Becak')
let bus = new Car('Minibus', 250)
// let unknown = new Vehicle('unknown', 1, false)

let vehicles = []
vehicles.push(sedan)
vehicles.push(becak)
vehicles.push(bus)

// for loop ...
for (let i in vehicles) {
    let vehicle = vehicles[i]
    if (vehicle instanceof Car) {
        vehicle.startEngine()
    }

    vehicle.run()
}

// sedan.startEngine()
// sedan.run()
// sedan.stop()

// becak.run()
// becak.stop()

// bus.startEngine()
// bus.run()
// bus.stop()