class Vehicle {
    hasEngine = false
    speed = 1
    name = "unknown"

    constructor(name, speed, hasEngine) {
        if (this.constructor === Vehicle) {
            throw new Error("cannot instantiate from abstract class")
        }
        this.name = name
        this.speed = speed
        this.hasEngine = hasEngine
    }

    run() {
        console.log(`${this.name} running...`)
    }

    stop() {
        console.log(`${this.name} stopped!`)
    }
}