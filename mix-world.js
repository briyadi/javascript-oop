const wiranto = new Police('Wiranto', 'Jakarta')
const prabowo = new Army('Prabowo', 'Jakarta')
const boyke = new Doctor('Boyke', 'Jakarta')
const sabrina = new Writer('Sabrina', 'Jakarta')

// console.log(wiranto instanceof Human)
// console.log(prabowo instanceof Human)
// console.log(boyke instanceof Human)
// console.log(sabrina instanceof Human)
wiranto.save()
prabowo.save()
boyke.save()
// sabrina.save()

wiranto.shoot()
prabowo.shoot()
// boyke.shoot()