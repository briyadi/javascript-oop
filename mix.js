class Human {
    constructor(name, address) {
        if (this.constructor === Human) {
            throw new Error("Human is abstract class")
        }
        this.name = name
        this.address = address
    }

    introduce() {
        console.log(`hi my name is ${this.name}`)
    }

    work() {
        console.log(`Work!`)
    }
}

const PublicServer = Base => class extends Base {
    save() {
        console.log(`Saving...`)
    }
}

const Military = Base => class extends Base {
    shoot() {
        console.log(`DOOR!`)
    }
}