class Car extends Vehicle {

    engineStarted = false

    constructor(name, speed) {
        super(name, speed, true)
    }

    startEngine() {
        this.engineStarted = true
        console.log(`engine started!`)
    }

    // overriding
    run() {
        if (!this.engineStarted) {
            console.error(`${this.name} is not started`)
            return
        }

        super.run()
    }
}